package main

import "log"

func main() {
	/*archivo := "prueba.txt"

	f, err := os.Open(archivo)

	defer f.Close()
	// defer ejecuta la funcion si o si
	// se ejecuta antes de salir de la función

	if err != nil{
		fmt.Println("error abriendo archivo")
		os.Exit(1)
	}*/
	ejemplopanic()

}

func ejemplopanic() {
	defer func() {
		recov := recover()
		if recov != nil {
			log.Fatalf("Hubo un error que genero panic \n %v", recov)
			//Fatal aparte de grabar en logs e imprimir ejecuta un os.Exit(1)
			//y eso lo diferencia de printf
		}
	}()
	a := 1

	if a == 1 {
		panic("se encontro el valor de 1")
	}

}
