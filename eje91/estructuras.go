package main

import (
	us "Curso_go_complete/eje91/user"
	"fmt"
	"time"
)

/*type usuario struct {
	Id        int
	Nombre    string
	FechaAlta time.Time
	Status    bool
} */
//definicion de como crear un objeto

type pepe struct {
	us.Usuario
}

func main() {
	u := new(pepe)
	u.AltaUsuario(1, "daniel", time.Now(), true)

	fmt.Println(u.Usuario)
}
