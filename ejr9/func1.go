package main

import (
	"fmt"
)

func main() {
	fmt.Println(Calculo(5, 6, 6, 6, 8, 9, 12, 144))

}

func Calculo(numero ...int) int {
	total := 0
	for item, num := range numero {
		total = total + num
		fmt.Printf("\n item %d \n", item)
	}
	return total
}
