package main

import "fmt"

func main() {
	// paises := make(map[string]string)
	// paises["Mexico"] = "cdmx"
	// paises["Argentina"] = "Buenos Aires"
	// fmt.Println(paises["Mexico"])

	campeonato := map[string]int{
		"Barcelona":    13,
		"Real Madrid":  38,
		"chivas":       56,
		"Boca Juniors": 30,
	}

	campeonato["River"] = 25
	campeonato["chivas"] = 100        // podemos modificar
	delete(campeonato, "Real Madrid") // para eliminar de un mapa
	fmt.Println(campeonato)

	//recorremos el map
	for equipo, puntaje := range campeonato {
		fmt.Printf("El equipo %s,tiene un puntaje de: %d\n", equipo, puntaje)
	}
	puntaje, existe := campeonato["chivas"] // evaluacion si existe un equipo en el mapa
	fmt.Printf("El puntaje capturado es %d, y el equipo existe %t\n", puntaje, existe)
}
