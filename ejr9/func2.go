package main

import "fmt"

var Calculo func(int, int) int = func(num1 int, num2 int) int {
	return num1 + num2
}

func main() {
	fmt.Printf("Suma 5 + 7  = %d \n ", Calculo(5, 7))

	// vamos a restar redefiniendo la variable calculo

	Calculo = func(n1 int, n2 int) int {
		return n1 - n2
	}

	fmt.Printf("Resta 15 - 7 = %d \n ", Calculo(15, 7))
	Operaciones()

	/*closures*/
	tabla2 := 2
	Mitabla := Tabla(tabla2)

	for i := 1; i < 11; i++ {
		fmt.Println(Mitabla())
	}

}

func Operaciones() {
	resultado := func() int {
		var a int = 23
		var b int = 14
		return a + b

	}
	fmt.Println(resultado())
}

func Tabla(valor int) func() int {

	numero := valor
	secuencia := 0

	return func() int {
		secuencia++
		return numero * secuencia
	}

}
