package main

import "fmt"

func main() {
	matriz := []int{2, 5, 4}
	fmt.Println(matriz)
	variante2()
	variante3()
	variante4()
}

func variante2() {
	elementos := [5]int{1, 2, 3, 4, 5}
	/*desde la posicion n hasta la ultima [n:] o [n:n]*/
	porcion := elementos[3:] // creamos slice a partir de un elemento mas grande "Elemento"

	fmt.Println(porcion)

}

func variante3() {
	/*Crear un slice*/
	//el terner parametro indica la capacidad maxima de un slice
	elementos := make([]int, 5, 20)

	fmt.Printf("Largo %d, capacidad %d \n", len(elementos), cap(elementos))

}

func variante4() {
	nums := make([]int, 0, 0)
	for i := 0; i < 100; i++ {
		nums = append(nums, i)
	}
	fmt.Printf("\nLargo %d, capacidad %d \n", len(nums), cap(nums))
}
