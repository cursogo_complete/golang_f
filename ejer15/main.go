package main

import (
	"fmt"
	"time"
)

func main() {

	// channel: Es un espacio de memoria , en el que vana a dilogar varias rutinas

	canal1 := make(chan time.Duration)

	go bucle(canal1)
	fmt.Println("LLegue hasta aqui")
	// pra decir que estoy escuchando el canal uno
	msg := <-canal1
	// en msg se grabara el resultado de canal 1
	// el programa no continua hasta que esta asignacion ocurra
	// (parecido al awair de node)
	fmt.Println(msg)
}

func bucle(canal chan time.Duration) {
	inicio := time.Now()

	for i := 0; i < 100000000000; i++ {

	}
	final := time.Now()
	canal <- final.Sub(inicio) // sub nos retorna la duracion
	// le asignamos al canal uno el valor resultante de la funcion ue en este caso
	// es la duracion
}
