package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	leoarchivo()
	leoarchivo2()
	graboarchivo()
	graboarchivo2()
}

func leoarchivo() {

	archivo, err := ioutil.ReadFile("./ejer11/archivo.txt")

	if err != nil {
		fmt.Println("Error Compa!!!")
	} else {
		fmt.Println(string(archivo)) // utilizamos string como función de conversion
	}

}

func leoarchivo2() {
	archivo, err := os.Open("./ejer11/archivo.txt")

	if err != nil {
		fmt.Println("Error Compa!!!")
	} else {
		scanner := bufio.NewScanner(archivo) // funcion para la lectura de archivos

		//vamos a poder recorrer secuenciamente registro por registro el arc hivo
		for scanner.Scan() { // -> aqui leemos la linea y la pasa al buffer
			registro := scanner.Text()               // aqui convertimos la linea guardada en el buffer a cadena de texto
			fmt.Printf("Linea > " + registro + "\n") // ->accedemos a la variable donde se guardoi la cadena convertida
		}

	}
	archivo.Close()
}

func graboarchivo() {

	archivo, err := os.Create("./ejer11/nuevoarchivo.txt")
	if err != nil {
		fmt.Println("Error Compa!!!")
		return
	}

	fmt.Fprintln(archivo, "ESTA ES UNA LINEA MAMALONA")
	archivo.Close()
}

func graboarchivo2() {

	fileName := "./ejer11/nuevoarchivo.txt"

	if Append(fileName, "\nesta es una segundalinea mamalona") == false {
		fmt.Println("Error compa")
	}

}

func Append(archivo string, texto string) bool {
	// O_WRONLY -> grabar y escribir , permiso
	// O_APPEND -> abrir el archivo sin limpiar lo que permite agregar

	arch, err := os.OpenFile(archivo, os.O_WRONLY|os.O_APPEND, 0644)

	if err != nil {
		fmt.Println("Error Compa!!!")
		return false
	}

	_, err = arch.WriteString(texto)

	if err != nil {
		fmt.Println("Error Compa!!!")
		return false
	}
	return true
}
