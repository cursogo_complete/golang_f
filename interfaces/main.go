package main

import "fmt"

type humano interface {
	pensar()
	sexo() string
	respirar()
}

type persona struct {
	edad      int
	altura    float32
	repirando bool
}

///para implementar una interface debo de suscricirlo a todas las fuciones
// que se definen en la interface

func (p *persona) pensar() {
	p.altura = 1.86
}

func (p *persona) respirar() {
	p.repirando = true
	fmt.Println("Estoy respirando", p.repirando)
}

func (p *persona) sexo() string {
	return "Hombre"
}

func Estavivo(h humano) {
	h.respirar()
	fmt.Println("Estoy vivo prro y soy", h.sexo())

}
func main() {
	Pedro := new(persona)
	Estavivo(Pedro)
}
